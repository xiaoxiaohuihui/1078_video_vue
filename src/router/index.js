import { createRouter, createWebHashHistory } from "vue-router";
const router = createRouter({
    history: createWebHashHistory("/"),
    routes: [
        {
            name: '首页',
            path: '/index',
            component: () => import('../views/index.vue'),
            meta: {
                auth: true,
              },
        },
        {
            name: '登录',
            path: '/',
            component: () => import('../views/login.vue'),
            meta: {
                auth: false,
              },
        },
        {
            name: 'rtvs',
            path: '/video',
            component: () => import('../views/video.vue'),
            meta: {
                auth: true,
              },
        },
        {
            name: '系统',
            path: '/system',
            component: () => import('../views/system.vue'),
            meta: {
                auth: true,
              },
        },
        {
            name: '回放视频',
            path: '/back',
            component: () => import('../views/back.vue'),
            meta: {
                auth: true,
              },
        },
        {
            name: '实时视频',
            path: '/storage',
            component: () => import('../views/flvs.vue'),
            meta: {
                auth: true,
              },
        },
        {
            name: '车辆信息',
            path: '/vehicle',
            component: () => import('../views/vehicle.vue'),
            meta: {
                auth: true,
              },
        },

    ],
});
router.beforeEach((to, from, next) => {
    // 1. 每个条件执行后都要跟上 next() 或 使用路由跳转 api 否则页面就会停留一动不动
    // 2. 要合理的搭配条件语句，避免出现路由死循环。
    const token = localStorage.getItem('token')
    if(to.path=="/"&&token!=null){
        router.replace("/index")
        next()
    }
    else if (to.meta.auth&&token==null){
        router.replace("/")
        next()
    } else{
        next()
    }
   
})

export default router;
