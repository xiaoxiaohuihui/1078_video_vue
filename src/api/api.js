import request from '../util/request' // 引入刚刚创建的域名文件
const base = '/instruction';// 解决跨域问题
const api = {
    //登陆
    getLogin(username, password) {
        return request.get(`${base}/user/token`, {
            params: {
                jtkeyId: username,
                jtKeySecret: password
            }
        })
    },
    // 在线车辆
    getCarsList() {
        return request.get(`${base}/other/terminal/carall`)
    },
    // 视频播放 
    getRealTimeVideo(clientId, channelNo, mediaType, streamType) {
        return request.get(`${base}/media/defaultrealtime/play`, {
            params: {
                clientId: clientId,
                channelNo: channelNo,
                mediaType: mediaType,
                streamType: streamType
            }
        })
    },
    //关闭视频
    closeVideo(clientId, channelNo, tag,) {
        return request.get(`${base}/user/video`, {
            params: {
                clientId: clientId,
                channelNo: channelNo,
                tag: tag
            }
        })
    },
    //查询回放录像时间
    selectBackTime(clientId, channelNo, startTime, endTime, warnBit1, warnBit2, mediaType, streamType, storageType) {
        return request.get(`${base}/media/file/search`, {
            params: {
                clientId: clientId,
                channelNo: channelNo,
                startTime: startTime,
                endTime: endTime,
                warnBit1: warnBit1,
                warnBit2: warnBit2,
                mediaType: mediaType,
                streamType: streamType,
                storageType: storageType
            }
        })
    },
    //查询历史回放轨迹
    selectBackPath(clientId, date, startTime, endTime) {
        return request.get(`${base}/user/getHistoricalTrack`, {
            params: {
                clientId: clientId,
                date: date,
                startTime: startTime,
                endTime: endTime
            }
        })
    },

};
export default api